package algorithms;

public class Assignment1RecipeCaesarSalad {

	public static void main(String[] args) {
		System.out.println("Caesar Salad\n"
				+ "		Step 1: Prepper tools and buy ingredients.\n"
				+ "		Step 2: Wash romaine lettuce, then chop it into bite-size pieces and spin it dry in a salad spinner.\n"
				+ "		Step 3: Combine Acids. Mix together Juice two large lemons, then top off with 1/2 lemon's worth of cider vinegar.\n"
				+ "		Step 4: Add Garlic.\n"
				+ "		Step 5: Add Egg Yolks.\n"
				+ "		Step 6: Add Mustard.\n"
				+ "		Step 7: Add Fresh Ground Pepper.\n"
				+ "		Step 8: Add Anchovy Fillets.\n"
				+ "		Step 9: Add a teaspoon of Worchestershire sauce.\n"
				+ "		Step 10: Mix and Serve.");

	}

}
