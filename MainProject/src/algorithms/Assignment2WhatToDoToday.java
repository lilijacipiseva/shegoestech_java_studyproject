package algorithms;

import java.util.Scanner;

/*
 Assignment No.2
Create an algorithm - what to do today!
An information:
Mary woke up at 7 AM. She asked herself - is this day a holiday? She can�t remember. If this day is a working day - need to go to work. If this day is a holiday - need to take a look at the weather forecast. If the forecasts show the possibility of rain - need to go to the Shopping center, if not - need to go to the beach.
How many options Mary has? - Mary has 3 options.
Create a flowchart of all options of Mary activities.
 */

public class Assignment2WhatToDoToday {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		boolean isHoliday;
		boolean isRain;

		System.out.println("Mary woke up at 7 AM");
		System.out.println("Is day a holiday?");
		isHoliday = input.nextBoolean();
		if (isHoliday == true) {
			System.out.println("Mary looks at the weather forecast.\nIf there is the possibility of rain?");
			isRain = input.nextBoolean();
			if (isRain == true) {
				System.out.println("Mary needs to go to the Shopping center.");
			} else {
				System.out.println("Mary needs to go to the beach.");
			}

		} else {
			System.out.println("Mary  needs to go to work.");
		}

	}

}
