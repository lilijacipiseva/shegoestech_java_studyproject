package algorithms;
/*
 Assignment No.3.1 Create and algorithm how to find out which is larger 2^3 or 3^2 
 */

import java.util.Scanner;

public class Assignment31FindLarger {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int x;
		int y;
		int xPow;
		int yPow;

		System.out.println("Input number x ");
		x = input.nextInt();
		System.out.println("Input power for x ");
		xPow = input.nextInt();
		int xResult = (int) Math.pow(x, xPow);
		System.out.println(x + "^" + xPow + " = " + xResult);

		System.out.println("Input number y ");
		y = input.nextInt();
		System.out.println("Input power for y ");
		yPow = input.nextInt();
		int yResult = (int) Math.pow(y, yPow);
		System.out.println(y + "^" + yPow + " = " + yResult);

		if (xResult == yResult) {
			System.out.println(x + "^" + xPow + " is equal to " + y + "^" + yPow);
		} else if (xResult > yResult) {
			System.out.println(x + "^" + xPow + " is larger than " + y + "^" + yPow);
		} else {
			System.out.println(y + "^" + yPow + " is larger than " + x + "^" + xPow);
		}

	}

}
