package algorithms;

import java.util.Scanner;

public class Assignment32SolveEquation {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		double x;
		double y;
		int a;
		int b;
		int c;
		int d;
		int e;
		int f;

		System.out.println("ax  + by = e & cx + dy = f ");
		System.out.println("Input  a, b, c, d, e, f integer numbers");
		a = input.nextInt();
		System.out.println();
		b = input.nextInt();
		System.out.println();
		c = input.nextInt();
		System.out.println();
		d = input.nextInt();
		System.out.println();
		e = input.nextInt();
		System.out.println();
		f = input.nextInt();
		System.out.println(a + "x  + " + b + "y = " + e + " & " + c + "x + " + d + "y = " + f);

		if ((a * d - b * c) != 0) {
			x = (e * d - b * f) / (a * d - b * c);
			y = (a * f - e * c) / (a * d - b * c);
			System.out.println("x = " + x);
			System.out.println("y = " + y);
		} else {
			System.out.println("The equation has no solution");
		}

	}

}
