package assgmntOfficer;

import java.util.ArrayList;

public class Execution {

	public static void main(String[] args) {
		Officer[] officers = new Officer[7];
//		District[] districts = new District[2];

		for (int i = 0; i < officers.length; i++)
			officers[i] = new Officer();

		System.out.println("The first officer: ");
		System.out.println(officers[0] = new Officer("John", "Davies", 1567, 16));
		System.out.println("The second officer: ");
		System.out.println(officers[1] = new Officer("William", "Brown", 4583, 24));
		System.out.println("The third officer: ");
		System.out.println(officers[2] = new Officer("David", "Wrighr", 8693, 43));
		System.out.println("The fourth officer: ");
		System.out.println(officers[3] = new Officer("Richard", "Green", 2367, 27));
		System.out.println("The fifth officer: ");
		System.out.println(officers[4] = new Officer("Peter", "Clarke", 5738, 41));
		System.out.println("The sixth officer: ");
		System.out.println(officers[5] = new Officer("Robert", "Clain", 1756, 21));
		System.out.println("The seventh officer: ");
		System.out.println(officers[6] = new Officer("Leo", "Johnson", 4783, 24));

		District district1 = new District("District51", "New-York", 12);
		District district2 = new District("District21", "Washington", 22);

		for (int i = 0; i < 3; i++)
			district1.addNewOfficer(officers[i]);

		for (int i = 3; i < 7; i++)
			district2.addNewOfficer(officers[i]);

		System.out.println(district1);
		System.out.println(System.lineSeparator());
		System.out.println(district2);
		System.out.println(System.lineSeparator());

		district2.removerOfficer(officers[6]);
		System.out.println("One officer was removed from the second District");
		System.out.println(district2);
		System.out.println(System.lineSeparator());

		System.out.println("Average level in the first district: " + district1.calculateAvgLevelInDistrict());
		System.out.println(System.lineSeparator());

		System.out.println("Average level in the second district: " + district2.calculateAvgLevelInDistrict());
		System.out.println(System.lineSeparator());

		System.out.println(
				"There are " + district1.getOfficersInTheDistrict().size() + " officers in the first district");

		System.out.println(
				"There are " + district2.getOfficersInTheDistrict().size() + " officers in the second district");

		ArrayList<Officer> officersInTheFirstDistrict = district1.getOfficersInTheDistrict();
		ArrayList<Officer> officersInTheSecondDistrict = district2.getOfficersInTheDistrict();
		District commonDistrict = new District("District1", "London", 323);
		commonDistrict.getOfficersInTheDistrict().addAll(officersInTheFirstDistrict);
		commonDistrict.getOfficersInTheDistrict().addAll(officersInTheSecondDistrict);

		System.out.println("Average level of both districts: " + commonDistrict.calculateAvgLevelInDistrict());
		System.out.println(
				"There are " + commonDistrict.getOfficersInTheDistrict().size() + " officers in Common district");

		if (district1.calculateAvgLevelInDistrict() == district2.calculateAvgLevelInDistrict())
			System.out.println("Levels are the same");
		else
			System.out.println((district1.calculateAvgLevelInDistrict() < district2.calculateAvgLevelInDistrict())
					? "District 2 has better average level"
					: "District 1 has better average level");
	}

}
