package assgmntOfficerHometask;

import java.util.ArrayList;
import java.util.Iterator;

import assgmntOfficer.District;
import assgmntOfficer.Officer;

public class Execution {

	public static void main(String[] args) {

		Officer officers1 = new Officer("John", "Davies", 1567, 16);
		Officer officers2 = new Officer("William", "Brown", 4583, 24);
		Officer officers3 = new Officer("David", "Wrighr", 8693, 43);
		Officer officers4 = new Officer("Richard", "Green", 2367, 27);
		Officer officers5 = new Officer("Peter", "Clarke", 5738, 41);
		Officer officers6 = new Officer("Robert", "Clain", 1756, 21);
		Officer officers7 = new Officer("Leo", "Johnson", 4783, 24);

		District district1 = new District("District51", "New-York", 12);
		District district2 = new District("District21", "Washington", 22);

		Lawyer lawyer1 = new Lawyer("Gerald", "Carter", 44678, 12);
		Lawyer lawyer2 = new Lawyer("Crispin", "Nash", 42875, 24);
		Lawyer lawyer3 = new Lawyer("Gunner", "Paris", 45947, 36);

		Officer[] officers = new Officer[7];

		for (int i = 0; i < officers.length; i++)
			officers[i] = new Officer();

		System.out.println("The first officer: ");
		System.out.println(officers[0] = officers1);
		System.out.println("The second officer: ");
		System.out.println(officers[1] = officers2);
		System.out.println("The third officer: ");
		System.out.println(officers[2] = officers3);
		System.out.println("The fourth officer: ");
		System.out.println(officers[3] = officers4);
		System.out.println("The fifth officer: ");
		System.out.println(officers[4] = officers5);
		System.out.println("The sixth officer: ");
		System.out.println(officers[5] = officers6);
		System.out.println("The seventh officer: ");
		System.out.println(officers[6] = officers7);

		for (int i = 0; i < 3; i++)
			district1.addNewOfficer(officers[i]);

		for (int i = 3; i < 7; i++)
			district2.addNewOfficer(officers[i]);
		
		System.out.println(district1);
		System.out.println(System.lineSeparator());
		System.out.println(district2);
		System.out.println(System.lineSeparator());
		
		System.out.println("The first lawyer: ");
		System.out.println(lawyer1);
		System.out.println("The second lawyer: ");
		System.out.println(lawyer2);
		System.out.println("The third lawyer: ");
		System.out.println(lawyer3);
		
		ArrayList<Lawyer> lawyers = new ArrayList<>();
		lawyers.add(lawyer1);
		lawyers.add(lawyer2);
		lawyers.add(lawyer3);
//		System.out.println(lawyers);
		
		int crimesSum = 0;
		Iterator<Lawyer> iterator1 = lawyers.iterator();
		while (iterator1.hasNext()) {
			crimesSum += iterator1.next().getHelpedInCrimesSolving();			
		}
		System.out.println("Total number of the crimes is " + crimesSum);
		
		Lawyer bestLawyer = null;
		Iterator<Lawyer> iterator2 = lawyers.iterator();
		while (iterator2.hasNext()) {
			Lawyer currentLawyer = iterator2.next();
			if (bestLawyer == null || bestLawyer.getHelpedInCrimesSolving() < currentLawyer.getHelpedInCrimesSolving())		
		bestLawyer = currentLawyer;
		}
		System.out.println(bestLawyer.getName() + " is the best lawyer");
	}

}
