package assgmntOfficerHometask;

import java.util.ArrayList;

public class Lawyer extends Person {

//	private String name, surname;
	private int lawyerID, helpedInCrimesSolving;

	public Lawyer() {

	}

	public Lawyer(String name, String surname, int lawyerID, int helpedInCrimesSolving) {
//		this.name = name;
//		this.surname = surname;
		super(name, surname);
		this.lawyerID = lawyerID;
		this.helpedInCrimesSolving = helpedInCrimesSolving;
	}

	private ArrayList<Lawyer> lawyers = new ArrayList<Lawyer>();// We create array list

//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getSurname() {
//		return surname;
//	}
//
//	public void setSurname(String surname) {
//		this.surname = surname;
//	}

	public ArrayList<Lawyer> getLawyers() {
		return lawyers;
	}

	public void setLawyers(ArrayList<Lawyer> lawyers) {
		this.lawyers = lawyers;
	}

	public int getLawyerID() {
		return lawyerID;
	}

	public void setLawyerID(int lawyerID) {
		this.lawyerID = lawyerID;
	}

	public int getHelpedInCrimesSolving() {
		return helpedInCrimesSolving;
	}

	public void setHelpedInCrimesSolving(int helpedInCrimesSolving) {
		this.helpedInCrimesSolving = helpedInCrimesSolving;
	}

	@Override
	public String toString() {
		return super.toString() + "LawyerID: " + this.lawyerID + System.lineSeparator() + "Helped In Crimes Solving: "
				+ this.helpedInCrimesSolving + System.lineSeparator();
	}

}
