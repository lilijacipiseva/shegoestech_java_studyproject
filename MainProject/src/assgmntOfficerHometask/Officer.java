package assgmntOfficerHometask;

public class Officer extends Person {

//	private String name, surname;
	private int officerID, crimesSolved;

	public Officer() {

	}

	public Officer(String name, String surname, int officerID, int crimesSolved) {
//		this.name = name;
//		this.surname = surname;
		super(name, surname);
		this.crimesSolved = crimesSolved;
		this.officerID = officerID;
	}

//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getSurname() {
//		return surname;
//	}
//
//	public void setSurname(String surname) {
//		this.surname = surname;
//	}

	public int getOfficerID() {
		return officerID;
	}

	public void setOfficerID(int officerID) {
		this.officerID = officerID;
	}

	public int getCrimasSolved() {
		return crimesSolved;
	}

	public void setCrimasSolved(int crimesSolved) {
		this.crimesSolved = crimesSolved;
	}

	@Override
	public String toString() {
		return super.toString() + "OfficerID: " + this.officerID + System.lineSeparator() + "Crimes Solved: "
				+ this.crimesSolved + System.lineSeparator();
	}

	public int calculateLevel() {
		if (this.crimesSolved < 20)
			return 1;
		else if (this.crimesSolved >= 20 && this.crimesSolved < 40)
			return 2;
		else
			return 3;
	}

}
