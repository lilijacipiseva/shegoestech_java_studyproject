package assgmntOfficerLawyer;

import java.util.ArrayList;
import java.util.Iterator;

public class District {

	private String title, city;
	private int districtID;

	private ArrayList<Person> personsInTheDistrict = new ArrayList<Person>();// We create array list

	public ArrayList<Person> getPersonsInTheDistrict() {
		return personsInTheDistrict;
	}

	public void setPersonsInTheDistrict(ArrayList<Person> personsInTheDistrict) {
		this.personsInTheDistrict = personsInTheDistrict;
	}
	
	public District() {
		
	}

	public District(String title, String city, int districtID) {
		this.title = title;
		this.city = city;
		this.districtID = districtID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getDistrictID() {
		return districtID;
	}

	public void setDistrictID(int districtID) {
		this.districtID = districtID;
	}

	@Override
	public String toString() {
		return "The title: " + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator()
				+ "District ID: " + this.districtID + System.lineSeparator() + "There are "
				+ this.personsInTheDistrict.size() + " persons in the district";
	}

	public void addNewPerson(Person person) {
		this.personsInTheDistrict.add(person);
	}

	public void removerPerson(Person person) {
		this.personsInTheDistrict.remove(person);
	}
	
//d.	Edit the method calculateAvgLevelInDistrict() that only all Officers’ levels are summarized and 
//	divided by the amount of the Officers (not Lawyers) in this District.
	
	public float calculateAvgLevelInDistrict() {
		int levelTotal = 0, numberOfOfficer = 0;
		Iterator<Person> iterator = this.personsInTheDistrict.iterator();
		while (iterator.hasNext()) {
			Person person = iterator.next();
			if (person instanceof assgmntOfficerLawyer.Officer) {
				assgmntOfficerLawyer.Officer officer = (assgmntOfficerLawyer.Officer) person;				
				numberOfOfficer++;
				levelTotal += officer.calculateLevel();
			}
		}

		return (float) levelTotal / (float) numberOfOfficer;
	}

}
