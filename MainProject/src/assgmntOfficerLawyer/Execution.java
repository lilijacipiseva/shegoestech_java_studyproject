package assgmntOfficerLawyer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Execution {

	public static void main(String[] args) {

// a.	Create seven Officers as the objects, two Districts as the objects and 
//		three Lawyers as the objects.		

		Officer officers1 = new Officer("John", "Davies", 1567, 16);
		Officer officers2 = new Officer("William", "Brown", 4583, 24);
		Officer officers3 = new Officer("David", "Wrighr", 8693, 43);
		Officer officers4 = new Officer("Richard", "Green", 2367, 27);
		Officer officers5 = new Officer("Peter", "Clarke", 5738, 41);
		Officer officers6 = new Officer("Robert", "Clain", 1756, 21);
		Officer officers7 = new Officer("Leo", "Johnson", 4783, 24);

		Lawyer lawyer1 = new Lawyer("Gerald", "Carter", 44678, 12);
		Lawyer lawyer2 = new Lawyer("Crispin", "Nash", 42875, 24);
		Lawyer lawyer3 = new Lawyer("Gunner", "Paris", 45947, 36);

		District district1 = new District("District51", "New-York", 12);
		District district2 = new District("District21", "Washington", 22);

//b.	Add three Officers and two Lawyers in the first District and others Officers 
//		and Lawyers in the second District.
		ArrayList<Person> personsInDistict1 = new ArrayList<Person>();
		personsInDistict1.add(officers1);
		personsInDistict1.add(officers2);
		personsInDistict1.add(officers3);
		personsInDistict1.add(lawyer1);
		personsInDistict1.add(lawyer2);
		district1.setPersonsInTheDistrict(personsInDistict1);
//c.	Print out all information about each District.		
		System.out.println(district1);
		System.out.println(personsInDistict1);
		System.out.println(System.lineSeparator());

		System.out.println("The average level in the district " + district1.getTitle() + " is "
				+ district1.calculateAvgLevelInDistrict());
		System.out.println(System.lineSeparator());

		ArrayList<Person> personsInDistict2 = new ArrayList<Person>();
		personsInDistict2.add(officers4);
		personsInDistict2.add(officers5);
		personsInDistict2.add(officers6);
		personsInDistict2.add(officers7);
		personsInDistict2.add(lawyer3);
		district2.setPersonsInTheDistrict(personsInDistict2);
		System.out.println(district2);
		System.out.println(personsInDistict2);
		System.out.println(System.lineSeparator());
		System.out.println("The average level in the district " + district2.getTitle() + " is "
				+ district2.calculateAvgLevelInDistrict());
		System.out.println(System.lineSeparator());

		ArrayList<Person> personInCommonDistrict = new ArrayList<Person>();
		District commonDistrict = new District("District1", "Washington", 11);
		personInCommonDistrict.add(officers1);
		personInCommonDistrict.add(officers2);
		personInCommonDistrict.add(officers3);
		personInCommonDistrict.add(officers4);
		personInCommonDistrict.add(officers5);
		personInCommonDistrict.add(officers6);
		personInCommonDistrict.add(officers7);
		personInCommonDistrict.add(lawyer1);
		personInCommonDistrict.add(lawyer2);
		personInCommonDistrict.add(lawyer3);
		commonDistrict.setPersonsInTheDistrict(personInCommonDistrict);
		System.out.println(commonDistrict);
		System.out.println(System.lineSeparator());
		System.out.println("The average level in the district " + commonDistrict.getTitle() + " is "
				+ commonDistrict.calculateAvgLevelInDistrict());
		System.out.println(System.lineSeparator());

		ArrayList<District> newDistrict = new ArrayList<District>();
		newDistrict.add(district1);
		newDistrict.add(district2);
		System.out.println(newDistrict);

		ArrayList<Person> personInNewDistrict = new ArrayList<Person>();
		personInNewDistrict.addAll(personsInDistict1);
		personInNewDistrict.addAll(personsInDistict2);
		System.out.println(personInNewDistrict);

//e.	Create an arraylist for the Districts storage. Put both Districts in the arraylist. 		
		District district3 = new District("District3", "Washington", 3);
		district3.getPersonsInTheDistrict().addAll(district1.getPersonsInTheDistrict());
		district3.getPersonsInTheDistrict().addAll(district2.getPersonsInTheDistrict());
		System.out.println(district3);
		System.out.println(System.lineSeparator());

// f. Find out which District is with the highest amount of Persons.
		int numberOfPersonInDistrict1 = district1.getPersonsInTheDistrict().size();
		int numberOfPersonInDistrict2 = district2.getPersonsInTheDistrict().size();
		if (numberOfPersonInDistrict1 > numberOfPersonInDistrict2)
			System.out.println("The number of persons in district1 is haigher than in district2");
		else if (numberOfPersonInDistrict1 < numberOfPersonInDistrict2)
			System.out.println("The number of persons in district2 is haigher than in district1");
		else
			System.out.println("The number of persons in both districts is the same");
	}

}
