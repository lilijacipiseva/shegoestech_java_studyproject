package assgmntOfficerLawyer;

import java.util.ArrayList;


public class Lawyer extends Person {

	private int lawyerID, helpedInCrimesSolving;

	public Lawyer() {

	}

	public Lawyer(String name, String surname, int lawyerID, int helpedInCrimesSolving) {
		super(name, surname);
		this.lawyerID = lawyerID;
		this.helpedInCrimesSolving = helpedInCrimesSolving;
	}

	private ArrayList<Lawyer> lawyers = new ArrayList<Lawyer>();

	public ArrayList<Lawyer> getLawyers() {
		return lawyers;
	}

	public void setLawyers(ArrayList<Lawyer> lawyers) {
		this.lawyers = lawyers;
	}

	public int getLawyerID() {
		return lawyerID;
	}

	public void setLawyerID(int lawyerID) {
		this.lawyerID = lawyerID;
	}

	public int getHelpedInCrimesSolving() {
		return helpedInCrimesSolving;
	}

	public void setHelpedInCrimesSolving(int helpedInCrimesSolving) {
		this.helpedInCrimesSolving = helpedInCrimesSolving;
	}

	@Override
	public String toString() {
		return super.toString() + "LawyerID: " + this.lawyerID + System.lineSeparator() + "Helped In Crimes Solving: "
				+ this.helpedInCrimesSolving + System.lineSeparator();
	}


}
