package basics;

import java.util.*;

public class Country {
	
	public static String countryName;
	public static String capitalName;
	public static int population;
	public static boolean isInEU;
	public static double GDP;

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Enter Country name");
		countryName = myScanner.nextLine();
		
		System.out.println("Enter Capital name");
		capitalName = myScanner.nextLine();
		
		System.out.println("Enter population");
		Country.population = Integer.parseInt(myScanner.nextLine());
		
		System.out.println("Enter is the Country in EU");
		Country.isInEU = Boolean.parseBoolean(myScanner.nextLine());
		
		System.out.println("Enter GDP");
		Country.GDP = Double.parseDouble(myScanner.nextLine());
		myScanner.close();
		show();
	}	
		public static void show() {
			System.out.println("Country name " + countryName);
			System.out.println("Ccapital name " + capitalName);
			System.out.println("Population: " + Country.population);
			System.out.println("Country is in EU: " + Country.isInEU);
			System.out.println("Country GPD: " + Country.GDP);
		}
	}


