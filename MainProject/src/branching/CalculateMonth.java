package branching;
/*
 Assignment No 1.5
Write a program where calculation of month will be performed if day number of the year is given.
For example, if the day number is 10, it is January, if day number is 32 - February, if day number is 100 - April, etc.
Information shown in the console:
The day number is: 100
It is: April

*It is possible to read user input from the console. Please take a look at the Scanner class.
https://docs.oracle.com/javase/7/docs/api/java/util/Scanner.html
Please ask the user to write the day number in the console. Read it and calculate which month it is. 

 */

import java.util.Scanner;

public class CalculateMonth {

	public static void main(String[] args) {
		
		int day, year;
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Please write the day: ");
		day = Integer.parseInt(input.nextLine());
		System.out.print("Please write the year: ");
		year = Integer.parseInt(input.nextLine());
		
		System.out.println(monthColculation(day, year));

	}

	public static String monthColculation(int day, int year) {
		if (!dayValid(day, year)) {
			return "Day is not valid!";
		}
		if (!yearValid(year)) {
			return "Year is not valid!";
		}
		String month = "";
		int januaryDays = 31;
		int februaryDays = januaryDays + dayCountInFebruary(year);
		int marchDays = februaryDays + 31;
		int aprilDays = marchDays + 30;
		int mayDays = aprilDays + 31;
		int juneDays = mayDays + 30;
		int julyDays = juneDays + 31;
		int augustDays = julyDays + 31;
		int septemberDays = augustDays + 30;
		int octoberDays = septemberDays + 31;
		int novemberDays = octoberDays + 30;
		int decemberDays = novemberDays + 31;

		if (day<=januaryDays) {			
			month = "January";
		} else if (day<=februaryDays) {
			month = "February";
		}  else if (day<=marchDays) {
			month = "March";
		} else if (day<=aprilDays) {
			month = "April";
		} else if (day <= mayDays) {
            month = "May";
        } else if (day <= juneDays) {
            month = "June";
        } else if (day <= julyDays) {
            month = "July";
        } else if (day <= augustDays) {
            month = "August";
        } else if (day <= septemberDays) {
            month = "Septemeber";
        } else if (day <= octoberDays) {
            month = "October";
        } else if (day <= novemberDays) {
            month = "November";
        } else if (day<=decemberDays) {
            month = "December";
        }
		return "It is:" + month;
	}

	private static boolean dayValid(int day, int year) {
		if (day < 0 || day > dayCountInYear(year)) {
			return false;
		} else {
			return true;
		}
	}

	private static boolean yearValid(int year) {
		if (year < 0) {
			return false;
		} else
			return true;
	}

	private static boolean yearIsLeap(int year) {
		if ((year % 4 == 0) || (year % 400 == 0) && (year % 100 != 0)) {
			return true;
		} else
			return false;
	}

	private static int dayCountInFebruary(int year) {
		if (yearIsLeap(year)) {
			return 29;
		} else
			return 28;
	}
	
	private static int dayCountInYear(int year) {
		if (yearIsLeap(year)) {
			return 366;
		} else
			return 365;
	}

}
