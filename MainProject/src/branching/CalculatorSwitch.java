package branching;
/*
Assignment No.2.3
Write a program to do multiple calculation options with two numbers: 
●	Create two numerical variables with initial values.
●	Create an char type variable which represents the user option (please use the switch control):
a.	‘+’ sum both elements;
b.	’-’: do subtraction; 
c.	’/’: do dividing; 
d.	‘*’: do multiplication; 
e.	‘%’: get remainder when the first element is divided by the second element;
f.	‘p’: print out both elements;
g.	‘b’: verify which element is bigger;
h.	‘s’:verify which element is smaller;
●	* Please add the possibility to write both variable values and the option in the console. Read, verify and do an algorithm
*/

import java.util.Scanner;

public class CalculatorSwitch {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		char[] operators = { '+', '-', '*', '/', '%', 'p', 'b', 's' };
		for (char operator : operators) {
			calculator(operator, -1.2, 1.3);
		}

		System.out.println("Enter the first number:");
		double number1 = Double.parseDouble(myScanner.nextLine());

		System.out.println("Enter operator '+', '-', '*', '/', '%','p', 'b', 's'");
		char operator = myScanner.nextLine().charAt(0);

		System.out.println("Enter the second number:");
		double number2 = myScanner.nextDouble();

		calculator(operator, number1, number2);

		myScanner.close();
	}

	public static void calculator(char operator, double number1, double number2) {

		double result = 0;

		switch (operator) {
		case '+':
			result = number1 + number2;
			break;
		case '-':
			result = number1 - number2;
			break;
		case '/':
			result = number1 / number2;
			break;
		case '*':
			result = number1 * number2;
			break;
		case '%':
			result = number1 % number2;
			break;
		case 'p':
			String resultP = number1 + ", " + number2;
			System.out.println(number1 + " " + operator + " " + number2 + " = " + resultP);
			return;
		case 'b':
			result = Math.max(number1, number2);
			break;
		case 's':
			result = Math.min(number1, number2);
			break;
		default:
			System.out.println("You've entered " + operator + ", which is incorrect");
			return;
		}
		System.out.println(number1 + " " + operator + " " + number2 + " = " + result);

	}

}
