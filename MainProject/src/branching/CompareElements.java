package branching;
/*
Assignment No.1.1
Write a program to compare two elements:
1.	Create two variables with the type short.
2.	Initialize the variables with the values as You want.
3.	Verify which variable is the largest. Print out information in the console.
4.	Verify which variable is the smallest. Print out information in the console.
5.	Verify if both variables are equals. Print out information in the console.
6.	Verify if there are any variables which are odd (uneven). Print out information in the console.
7.	Verify if there are any variables which are even. Print out information in the console.
8.	Verify if there are any variables which are negative. Print out information in the console.
9.	Verify if there are any variables which are positive. Print out information in the console.
10.	Verify if there are any variables which are less than 100. Print out information in the console.
11.	Do the same steps with three variables

 */

public class CompareElements {

	public static void main(String[] args) {

		short var1 = -11, var2 = 0, var3 = 101;
		
		compareTwoVariables(var1, var2);
		variableOddOrEven(var1);
		variableOddOrEven(var2);
		variableOddOrEven(var3);
		variableNegativeOrPositive(var1);
		variableNegativeOrPositive(var2);
		variableNegativeOrPositive(var3);
		variableLessThanHundred(var1);
		variableLessThanHundred(var2);
		variableLessThanHundred(var3);
		compareThreeVariables(var1, var2, var3);
	}

	public static void compareTwoVariables(short var1, short var2) {
		if (var1 > var2) {
			System.out.println(var1 + " is larger than " + var2);
			System.out.println(var1 + " is the largest");
			System.out.println(var2 + " is the smallest");
		} else if (var2 > var1) {
			System.out.println(var2 + " is larger than " + var1);
			System.out.println(var2 + " is the largest");
			System.out.println(var1 + " is the smallest");
		} else
			System.out.println("Both variables are the same");
	}

	public static void variableOddOrEven(short var) {
		if (var % 2 == 0) {
			System.out.println("The number " + var + " is even");
		} else
			System.out.println("The number " + var + " is odd");
	}

	public static void variableNegativeOrPositive(short var) {
		if (var < 0) {
			System.out.println("The number " + var + " is negative");
		} else if (var > 0) {
			System.out.println("The number " + var + " is positive");
		} else
			System.out.println("The number " + var + " is neutral");

	}
	
	public static void variableLessThanHundred(short var) {
		if (var < 100) {
			System.out.println("The number " + var + " is less than 100");
		}else
			System.out.println("The number " + var + " is bigger or equel to 100");
	}
	
	public static void compareThreeVariables(short var1, short var2, short var3) {
		if ((var1 > var2) && (var1 > var3)) {
			System.out.println(var1 + " is larger than " + var2 + " and " + var3);
		} else if ((var2 > var1) && (var2 > var3)) {
			System.out.println(var2 + " is larger than " + var1 + " and " + var3);
		} else if ((var3 > var1) && (var3 > var2)) {
			System.out.println(var3 + " is larger than " + var1 + " and " + var2);
		}
		if ((var1 < var2) && (var1 < var3)) {
			System.out.println(var1 + " is smaller than " + var2 + " and " + var3);
		} else if ((var2 < var1) && (var2 < var3)) {
			System.out.println(var2 + " is smaller than " + var1 + " and " + var3);
		} else if ((var3 < var1) && (var3 < var2)) {
			System.out.println(var3 + " is smaller than " + var1 + " and " + var2);
		}
		if ((var1 == var2) && (var1 == var3)) {
			System.out.println("All variables are the same");
		} else if (var2 == var1) {
			System.out.println(var1 + " and " + var2 + " are the same");
		} else if (var2 == var3) {
			System.out.println(var2 + " and " + var3 + " are the same");
		} else if (var1 == var3) {
			System.out.println(var1 + " and " + var3 + " are the same");
		}
			
	}

}
