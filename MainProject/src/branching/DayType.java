package branching;
/*
Assignment No.2.1
Write a program where information about the day type is shown in the console.
●	Create an int type variable which represents the day number in the week.
●	Using switch control, please print out if the day is working day or holiday.
●	Try Your algorithm with the day number 2. The output should be “It is a working day”.
●	Try Your algorithm with the day number 6. The output should be “It is holiday”.
●	Try Your algorithm with the day number 10. Please add correct verification in the code!
●	Try Your algorithm with the day number -4. Please add correct verification in the code!
●	* Please add the possibility to write the day number in the console. Read it, verify it and do an algorithm.
 */

import java.util.Scanner;

public class DayType {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		printDayType(2);
		printDayType(6);
		printDayType(10);
		printDayType(-4);

		System.out.print("Please write the number of day: ");
		int dayNumber = Integer.parseInt(input.nextLine());
		printDayType(dayNumber);

		for (int i = 0; i < 9; i++) {
			printDayType(i);
		}

	}

	public static void printDayType(int day) {

		switch (day) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5: {
			System.out.println("Day number: " + day + ". It is a working day");
			break;
		}
		case 6:
		case 7: {
			System.out.println("Day number: " + day + ". It is holiday");
			break;
		}
		default:
			System.out.println("The number of week day: " + day + " is incorrect");
		}
	}
}
