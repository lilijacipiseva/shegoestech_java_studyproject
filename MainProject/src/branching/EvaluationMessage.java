package branching;
/*
Assignment No.2.2
Write a program where an evaluation message of the student exam is created.
●	Create a char type variable which represents the grade level of the exam, for example ‘A’, ‘B’, ‘C’ ….‘F’.
●	Using switch control, please print out how good the student was in the exam as following:
a.	If the grade level is ‘A’ or ‘B’, print out the message “Perfect! You are so clever!”.
b.	If the grade level is ‘C’, print out the message “Good! But You can do better!”.
c.	If the grade level is ‘D’ or ‘E’, print out the message “It is not good! You should study!”.
d.	If the grade level is ‘F’, print out the message “Fail! You need to repeat the exam!”.
●	Try Your algorithm with the grade level ‘B’. The output should be “Good! But You can do better!”.
●	Try Your algorithm with the grade level ‘F’. The output should be “Fail! You need to repeat the exam!”.
●	Try Your algorithm with the grade level ‘Z’. Please add the default case in the code!
●	* Please add the possibility to write the grade level in the console. Read it, verify it and do an algorithm.
 */

import java.util.Scanner;

public class EvaluationMessage {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		printEvaluationMessage('B');
		printEvaluationMessage('F');
		printEvaluationMessage('Z');

		System.out.print("Please write the level: ");
		char examLevel = input.next().charAt(0);
		printEvaluationMessage(examLevel);

		char[] levels = { 'A', 'B', 'C', 'D', 'E', 'F', '*' };
		for (char level : levels) {
			printEvaluationMessage(level);
		}
	}

	private static void printEvaluationMessage(char level) {

		switch (level) {
		case 'A':
		case 'B': {
			System.out.println("Perfect! You are so clever!");
			break;
		}
		case 'C': {
			System.out.println("Good! But You can do better!");
			break;
		}
		case 'D':
		case 'E': {
			System.out.println("It is not good! You should study!");
			break;
		}
		case 'F': {
			System.out.println("Fail! You need to repeat the exam!");
			break;
		}
		default:
			System.out.println(level + " level doesn't exist!");
		}
	}
}
