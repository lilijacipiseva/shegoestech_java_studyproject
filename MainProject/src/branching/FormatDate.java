package branching;

import java.util.Scanner;

/*
Write a program to format and print out the value of the day. 
1	Create variables day, month and year.
2	Initialize the variables with the values as You want.
4	Verify if day, month and year are correct. For example, day can not be -10, or month can not be 34, year can not be -2032. 
If the variables are incorrect, please inform the user. 
If all variables are in the correct range, please print out the day in the following format:
YYYY/MM/DD
5 It is possible to read user input from the console. 
Please take a look at the Scanner class. https://docs.oracle.com/javase/7/docs/api/java/util/Scanner.html
5.1 Please ask the user to write the day, month and year values in the console. Read them and store in the day, month and year variables.
5.2 Please ask the user in what format he/she wants to print out the day in the console: 1 - YYYY/MM/DD, 2- YYYY.MM.DD. 
Read the answer and do a branching in the code related to the user answer.

The output/input of the console may look like the following:
Please write the day: 23
Please write the month: 9
Please write the year: 2020
Please select the day formatting. 1 - YYYY/MM/DD, 2- YYYY.MM.DD: 1
Your day is 2020/9/23
 */
public class FormatDate {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int day = 1, month = 4, year = 2000, dateFormating = 2;
		printDate(day, month, year, dateFormating);

		day = -10;
		month = 34;
		year = -2032;
		dateFormating = 1;

		printDate(day, month, year, dateFormating);
		printDate(1, 13, 2000, 1);
		printDate(1, 12, 2000, 1);
		printDate(30, 2, 2000, 1);
		printDate(29, 2, 2001, 1);

		System.out.print("Please write the day: ");
		day = Integer.parseInt(input.nextLine());
		System.out.print("Please write the month: ");
		month = Integer.parseInt(input.nextLine());
		System.out.print("Please write the year: ");
		year = Integer.parseInt(input.nextLine());
		System.out.print("Please select the day formatting. 1 - YYYY/MM/DD, 2- YYYY.MM.DD: ");
		dateFormating = Integer.parseInt(input.nextLine());

		printDate(day, month, year, dateFormating);
	}

	public static boolean checkDayAndYearAreValid(int day, int year) {
		boolean correct = true;
		if (year < 0) {
			System.out.println("The year: " + year + " is incorrect. Year cannot be negative");
			correct = false;
		}
		if (day < 1) {
			System.out.println("The day: " + day + " is incorrect. Day cannot be negative or 0");
			correct = false;
		}
		return correct;
	}

	public static boolean checkDayAndMonthAreValid(int day, int month, int year) {
		boolean correct = true;
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			if (day > 31) {
				correct = false;
			}
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			if (day > 30) {
				correct = false;
			}
			break;
		case 2:
			if (year % 4 == 0) {
				if (day > 29) {
					correct = false;
				}
			} else if (day > 28) {
				correct = false;
			}
			break;
		default:
			System.out.println("The month: " + month + " is incorrect");
			correct = false;
			return correct;
		}
		if (!correct)
			System.out.println("The day: " + day + " is incorrect if month is " + month + " and year is " + year);
		return correct;
	}

	public static void printDate(int day, int month, int year, int dateFormating) {
		if (!checkDayAndYearAreValid(day, year)) {
			return;
		}

		if (!checkDayAndMonthAreValid(day, month, year)) {
			return;
		}

		switch (dateFormating) {
		case 1:
			System.out.println("Your date is " + year + "/" + month + "/" + day);
			break;
		case 2:
			System.out.println("Your date is " + year + "." + month + "." + day);
			break;
		default:
			System.out.println("Option " + dateFormating + " does not exist! Cab be 1 or 2.");
			break;
		}
	}

}
