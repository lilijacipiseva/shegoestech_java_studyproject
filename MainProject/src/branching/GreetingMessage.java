package branching;

/*
Write a program to show a greeting message in the console if time is known.
1	Create a variable time with value 8.
2	Verify the time value:
2.1	If the value is smaller than 12, then the greeting message �Good Morning Sunshine!� will be printed out in the console.
2.2	If the value is between 13 and 19, then the greeting message �Good Afternoon. Work Hard!� will be printed out in the console.
2.3	If the value is between 20 and 24, then the greeting message �Good Evening. Get some rest!� will be printed out in the console.
3	Change the time variable value to 20 and look at the console. Which greeting message is printed out in the console?
4	Change the time variable value to -2 and look at the console. Please add correct verification in the code!
5	Change the time variable value to 100 and look at the console. Please add correct verification in the code!
*/

public class GreetingMessage {

	public static void main(String[] args) {

		int time = 8;
		
		greetingByTime(time);
		greetingByTime(20);
		greetingByTime(-2);
		greetingByTime(100);
		greetingByTime(14);

	}

	public static void greetingByTime(int time) {
		String greeting;

		if (time <= 0 || time > 24) {
			System.out.println("Input time " + time + " is not valid, please, correct!");
			return;
		} else if (time <= 12)
			greeting = "Good Morning Sunshine!";
		else if (time > 12 && time <= 19)
			greeting = "Good Afternoon. Work Hard!";
		else
			greeting = "Good Evening. Get some rest!";
		System.out.println(greeting);
		
	}

}
