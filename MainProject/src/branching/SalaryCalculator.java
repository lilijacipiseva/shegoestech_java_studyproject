package branching;
/*
 Assignment No 1.6
Write a program where salary is calculated.
●	Create the variable workingHoursInDay.
●	Initialize the variable with the value as You want.
●	Please verify if the value is correct. For example, it is not possible to work -30 hours a day.
●	 Do a calculation of the salary:
a.	If the workingHoursInDay value is smaller or equals than 8, the hourly rate is 10 eur/h.
b.	If the workingHoursInDay value is larger than 8, the salary is 80 eur plus 15 eur for each extra hour. For example, if the workingHoursInDay is 10, the salary will be 
80 eur + (10-8)*15 eur = 110 eur.
●	Try Your algorithm with value 7. The result should be 70 eur.
●	Try Your algorithm with value 10. The result should be 110 eur.
●	Try Your algorithm with value -10. What does the algorithm do?
●	Please add the possibility to write the value of the workingHourInDay in the console. Read it, verify it and do an algorithm.

 */

import java.util.Scanner;

public class SalaryCalculator {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);

		int workingHoursInDay = 0;
		
		SalaryCalculation(7);
		SalaryCalculation(10);
		SalaryCalculation(-10);
		
		System.out.print("Please write the number of working hours: ");
		workingHoursInDay = Integer.parseInt(input.nextLine());
		SalaryCalculation(workingHoursInDay);
	}

	public static boolean checkWorkingHoursInDayIsValid(int hours) {
		boolean correct = true;
		if ((hours < 0) || (hours > 24)) {
			System.out.println("The number of working hours: " + hours + " is incorrect");
			correct = false;
		}
		return correct;
	}

	public static void SalaryCalculation(int hours) {
		if (!checkWorkingHoursInDayIsValid(hours)) {
			return;
		}
		if (hours <= 8) {
			System.out.println("Your salary is: " + hours * 10 + " EUR");
		}else {
			System.out.println("Your salary is: " + (80 + (hours - 8) * 15) + " EUR");
		}
	}

}
