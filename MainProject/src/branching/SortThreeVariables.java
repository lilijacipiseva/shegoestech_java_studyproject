package branching;

public class SortThreeVariables {

	public static void main(String[] args) {
		sortThreeVariables(5, 3, 1);
		sortThreeVariables(-10, -15, -13);
		sortThreeVariables(100, 50, 150);
		sortThreeVariables(50, 100, 30);
		sortThreeVariables(50, 100, 70);
		sortThreeVariables(50, 100, 150);
		sortThreeVariables(50, 50, 50);
		sortThreeVariables(100, 50, 50);
		sortThreeVariables(50, 100, 100);
		sortThreeVariables(100, 50, 100);
		sortThreeVariables(50, 100, 50);
		sortThreeVariables(50, 50, 100);
		sortThreeVariables(100, 100, 50);
	}

	public static void sortThreeVariables(int var1, int var2, int var3) {
		int max = 0, med = 0, min = 0;

		if (var1 > var2) {
			if (var1 > var3) {
				max = var1;
				if (var2 > var3) {
					med = var2;
					min = var3;
				} else {
					med = var3;
					min = var2;
				}
			} else {
				max = var3;
				med = var1;
				min = var2;
			}
		} else if (var2 > var3) {
			max = var2;
			if (var1 > var3) {
				max = var2;
				med = var1;
				min = var3;
			} else {
				max = var2;
				med = var3;
				min = var1;
			}
		} else {
			max = var3;
			med = var2;
			min = var1;
		}

		System.out.println("Sorted from min to max: " + min + ", " + med + ", " + max);
	}

}
