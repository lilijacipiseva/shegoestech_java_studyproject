package convertor;

import java.util.Scanner;

public class ConvertorSquare {

	public static void main(String[] args) {

		double valueSqM = inputSquareValue("Meters");
		convertSquareMetersToSquareKilometers(valueSqM);

		double valueSqKm = inputSquareValue("Kilometers");
		convertSquareKilometersToSquareMeters(valueSqKm);
	}

	public static double inputSquareValue(String name) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a value for " + name + ": ");
		return input.nextDouble();
	}

	public static void convertSquareMetersToSquareKilometers(double value) {
		double result = value / 1000000;
		System.out.println(value + " SquareMeters = " + result + " SquareKilometers");
	}

	public static void convertSquareKilometersToSquareMeters(double value) {
		double result = value * 1000000;
		System.out.println(value + " SquareKilometers = " + result + " SquareMeters");
	}

}
