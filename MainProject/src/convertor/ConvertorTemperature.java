package convertor;

import java.util.Scanner;

public class ConvertorTemperature {

	public static void main(String[] args) {

		double valueC = inputTemperature("Celsius");
		convertCelsiusToFahrenheit(valueC);

		double valueF = inputTemperature("Fahrenheit");
		convertFahrenheitToCelsius(valueF);

		double valueC1 = inputTemperature("Celsius");
		convertCelsiusToKelvin(valueC1);

		double valueK = inputTemperature("Kelvin");
		convertKelvinToCelsius(valueK);

	}

	public static double inputTemperature(String temperatureName) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a value for Temperature in " + temperatureName + ": ");
		double valueTemperature = input.nextDouble();
		return valueTemperature;
	}

	public static void convertCelsiusToFahrenheit(double value) {
		double result = value * 9 / 5 + 32;
		System.out.println(value + " Celsius = " + result + " Fahrenheit");
	}

	public static void convertFahrenheitToCelsius(double value) {
		double result = (value - 32) * 5 / 9;
		System.out.println(value + " Fahrenheit = " + result + " Celsius");
	}

	public static void convertCelsiusToKelvin(double value) {
		double result = value + 273.15;
		System.out.println(value + " Celsius = " + result + " Kelvin");
	}

	public static void convertKelvinToCelsius(double value) {
		double result = value - 273.15;
		System.out.println(value + " Kelvin = " + result + " Celsius");
	}

}
