package convertor;

import java.util.Scanner;

public class ConvertorTime {

	public static void main(String[] args) {

		int valueH = inputTime("Hours");
		convertHoursToDays(valueH);

		int valueD = inputTime("Days");
		convertDaysToHours(valueD);

	}

	public static int inputTime(String name) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a value for " + name + ": ");
		return input.nextInt();
	}

	public static void convertHoursToDays(int value) {
		double resultWithRemainder = (double) value / 24;
		int resultRemainder = value % 24;
		int resultWithoutRemainder = value / 24;
		System.out.println(value + " Hours = " + resultWithRemainder + " Days");
		System.out.println(value + " Hours = " + resultWithoutRemainder + " Days " + resultRemainder + " Hours");
	}

	public static void convertDaysToHours(int value) {
		int result = value * 24;
		System.out.println(value + " Days = " + result + " Hours");
	}
}
