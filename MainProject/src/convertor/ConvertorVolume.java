package convertor;

import java.util.Scanner;

public class ConvertorVolume {

	public static void main(String[] args) {

		double valueM = inputVolume("CubicMeters");
		convertCubicMetersToLiters(valueM);

		double valueL = inputVolume("Liters");
		convertLitersToCubicMeters(valueL);
	}

	public static double inputVolume(String name) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a value for " + name + ": ");
		return input.nextDouble();
	}

	public static void convertCubicMetersToLiters(double value) {
		double result = value * 1000;
		System.out.println(value + " CubicMeters = " + result + " Liters");
	}

	public static void convertLitersToCubicMeters(double value) {
		double result = value / 1000;
		System.out.println(value + " Liters = " + result + " CubicMeters");
	}

}
