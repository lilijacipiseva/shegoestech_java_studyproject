package convertor;

import java.util.Scanner;

public class ConvertorWeight {

	public static void main(String[] args) {

		double valueKg = inputWeight("Kilograms");
		convertKilogramToPound(valueKg);

		double valueP = inputWeight("Pounds");
		convertPoundToKilogram(valueP);
	}

	public static double inputWeight(String name) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a value for " + name + ": ");
		return input.nextDouble();
	}

	public static void convertKilogramToPound(double value) {
		double result = value * 2.205;
		System.out.println(value + " Kilograms = " + result + " Pounds");
	}

	public static void convertPoundToKilogram(double value) {
		double result = value / 2.205;
		System.out.println(value + " Pounds = " + result + " Kilograms");
	}

}
