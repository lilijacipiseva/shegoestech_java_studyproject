package convertor;

import java.util.Scanner;

public class CurrencyConvertor {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		double valueUSD = 0.00;
		double valueEUR = 0.00;
		
		System.out.println("Enter a value of USD: ");
		valueUSD = scanner.nextDouble();
		valueEUR = valueUSD * 0.821949;
		System.out.println(valueUSD + " USD = " + valueEUR + " EUR.");
		System.out.println("Conversion rate: 1 USD = 0.821949 EUR");
		
		System.out.println();
		
		System.out.println("Enter a value of EUR: ");
		valueEUR = scanner.nextDouble();
		valueUSD = valueEUR * 1.21662;
		System.out.println(valueEUR + " EUR = " + valueUSD + " USD.");
		System.out.println("Conversion rate: 1 EUR = 1.21662 USD");
		
		scanner.close();

	}

}
