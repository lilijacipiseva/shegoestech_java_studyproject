package convertor;

import java.util.Scanner;

public class MeterFeetConvertor {

	final static double feetToMeters = 0.305;
	final static double metersToFeet = 3.281;

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		double valueMeter;
		double valueFoot;

		System.out.println("Enter a value for meters: ");
		valueMeter = input.nextDouble();
		convertMetersToFeet(valueMeter);

		System.out.println("Enter a value for feet: ");
		valueFoot = input.nextDouble();
		convertFeetToMeters(valueFoot);

		input.close();

	}

	public static void convertMetersToFeet(double valueMeter) {
		double valueFoot = valueMeter * metersToFeet;
		System.out.println(valueMeter + " meters = " + valueFoot + " feet.");
	}

	public static void convertFeetToMeters(double valueFoot) {
		double valueMeter = valueFoot * metersToFeet;
		System.out.println(valueFoot + " feet = " + valueMeter + " meters.");
	}

}
