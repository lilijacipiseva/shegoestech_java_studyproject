package dataStructureAsgmnt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Asgmnt1 {

	public static void main(String[] args) {

//a.	Create a String array with following values: 
//		 “Computer”, “Plate”, “Chair”, “Girl”, “Boy”, “Cat”, “Dog”, “Shirt”, “Determination”;		
		List<String> strings = new ArrayList<String>();
		strings.add("Computer");
		strings.add("Plate");
		strings.add("Chair");
		strings.add("Girl");
		strings.add("Boy");
		strings.add("Cat");
		strings.add("Dog");
		strings.add("Shirt");
		strings.add("Determination");

//		b.	Find out how many values are starting with ‘C’;
//		c.	Find out how many values are ending with “e”;
//		d.	Find out how many values are with length 5;	
//		e.	Find out how many values consist of the character “e”;
//		f.	Find out is there any element which consists of the subString “te”;		
		int countStartC = 0;
		int countEndE = 0;
		int countLength5 = 0;
		int countCharE = 0;
		int countTe = 0;
		Iterator<String> iterator = strings.iterator();
		while (iterator.hasNext()) {
			String currentElements = iterator.next();
			if (currentElements.startsWith("C"))
				countStartC++;
			if (currentElements.endsWith("e"))
				countEndE++;
			if (currentElements.length() == 5)
				countLength5++;
			if (currentElements.contains("e"))
				countCharE++;
			if (currentElements.contains("te"))
				countTe++;
		}
		if (countStartC > 1)
			System.out.println("There are " + countStartC + " elements, starting with C");
		else if (countStartC == 1)
			System.out.println("There are " + countStartC + " elements, starting C");
		else {
			System.out.println("There are no elements, starting with C");
		}

		System.out.println("There are " + countEndE + " elements, ending with e");
		System.out.println("There are " + countLength5 + " elements, with length 5");
		System.out.println("There are " + countCharE + " elements, which contain character e");
		System.out.println("There are " + countTe + " elements, which consists of the subString te");
		System.out.println(System.lineSeparator());

//		g.	* Calculate a histogram of values related to length of value.

		Map<Integer, Integer> hist = new TreeMap<Integer, Integer>();
		iterator = strings.iterator();
		while (iterator.hasNext()) {
			String string = iterator.next();
			int len = string.length();
			if (hist.containsKey(len)) {
				int NumberOfTheElements = hist.get(len) + 1;
				hist.put(len, NumberOfTheElements);
			} else
				hist.put(len, 1);
		}

		Iterator<Integer> iteratorKeys = hist.keySet().iterator();
		while (iteratorKeys.hasNext()) {
			int len = iteratorKeys.next();
			System.out.println(len + " -> " + hist.get(len));
		}

//		●	Edit the previous written code to use ArrayList class for the value storing.
//		●	Sort all ArrayList values in ascending order by the first letter. 

		System.out.println(System.lineSeparator());
		printList(sort(strings));
	}

	private static List<String> sort(List<String> list) {
		List<String> sortedList = new ArrayList<String>(list);
		for (int i = 0; i < list.size(); i++) {
			for (int j = i + 1; j < list.size(); j++) {
				if (sortedList.get(i).compareTo(sortedList.get(j)) > 0) {
					String swap = sortedList.get(i);
					sortedList.set(i, sortedList.get(j));
					sortedList.set(j, swap);
				}
			}

		}

		return sortedList;
	}

	private static void printList(List<String> list) {
		Iterator<String> iterator = list.iterator();
		while (iterator.hasNext())
			System.out.println(iterator.next());
	}

}
