package dataStructureAsgmnt;
/*
●	Create a HashMap for the product price page where all products will be as the key and the price as the value.
●	Save at least 8 products in the HashMap.
●	Find out how many products are under 1 eur.
●	Find out which product price is the lowest.
●	Find out which product price is the largest.
●	Create a new HashMap with other products. Merge both HashMaps and print out how many products are in the merged HashMap.
●	*Sort all values in ascending order.
●	Repeat the same tasks with HashTable.

 */

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

public class Asgmnt2 {

	public static void main(String[] args) {

		Product jeans = new Product("jeans", new BigDecimal(120.));
		Product dress = new Product("dress", new BigDecimal(50));
		Product socks = new Product("socks", new BigDecimal("0.99"));
		Product skirt = new Product("skirt", new BigDecimal("0.50"));
		Product shorts = new Product("shorts", new BigDecimal(15));
		Product dungarees = new Product("dungarees", new BigDecimal(150));
		Product trousers = new Product("trousers", new BigDecimal(60));
		Product chinos = new Product("chinos", new BigDecimal(25));
		Product coat = new Product("coat", new BigDecimal(250));

		Map<String, BigDecimal> pricePage = new HashMap<String, BigDecimal>();
		pricePage.put(jeans.getName(), jeans.getPrice());
		pricePage.put(dress.getName(), dress.getPrice());
		pricePage.put(socks.getName(), socks.getPrice());
		pricePage.put(skirt.getName(), skirt.getPrice());
		pricePage.put(shorts.getName(), shorts.getPrice());
		pricePage.put(dungarees.getName(), dungarees.getPrice());
		pricePage.put(trousers.getName(), trousers.getPrice());
		pricePage.put(chinos.getName(), chinos.getPrice());
		jeans.setPrice(new BigDecimal(34));
		pricePage.put(jeans.getName(), jeans.getPrice());

		System.out.println("'Page price' " + pricePage.toString());
		System.out.println("'Page price' size: " + pricePage.size());

		Map<String, BigDecimal> pricePageCommon = new HashMap<String, BigDecimal>();
		pricePageCommon.put(coat.getName(), coat.getPrice());
		pricePageCommon.putAll(pricePage);

		System.out.println("'Common page price' " + pricePageCommon.toString());
		System.out.println("'Common page price' size: " + pricePageCommon.size());

		int countPriceUnderOne = 0;
		BigDecimal oneBigDecimal = new BigDecimal(1);
		BigDecimal maxBigDecimal = new BigDecimal(0);
		BigDecimal minBigDecimal = new BigDecimal(Integer.MAX_VALUE);

		for (Map.Entry entry : pricePage.entrySet()) {
			BigDecimal entryBigDecimal = (BigDecimal) entry.getValue();
			if (entryBigDecimal.compareTo(oneBigDecimal) == -1) {
				countPriceUnderOne++;
			}
			if (entryBigDecimal.compareTo(maxBigDecimal) == 1) {
				maxBigDecimal = entryBigDecimal;
			}
			if (entryBigDecimal.compareTo(minBigDecimal) == -1) {
				minBigDecimal = entryBigDecimal;
			}

		}
		System.out.println("Price under 1 EUR in 'page price' " + countPriceUnderOne);
		System.out.println("Max price in 'page price'  " + maxBigDecimal.toString());
		System.out.println("Min price in 'page price'  " + minBigDecimal.toString());

		List<BigDecimal> list = new ArrayList<>(pricePage.values());
		list.sort(null);
		System.out.println("Values of prices in 'page price' in ascending order" + list);

		Map<String, BigDecimal> pricePageTable = new Hashtable<String, BigDecimal>();
		pricePageTable.put(jeans.getName(), jeans.getPrice());
		pricePageTable.put(dress.getName(), dress.getPrice());
		pricePageTable.put(socks.getName(), socks.getPrice());
		pricePageTable.put(skirt.getName(), skirt.getPrice());
		pricePageTable.put(shorts.getName(), shorts.getPrice());
		pricePageTable.put(dungarees.getName(), dungarees.getPrice());
		pricePageTable.put(trousers.getName(), trousers.getPrice());
		pricePageTable.put(chinos.getName(), chinos.getPrice());

		System.out.println("'Page price table' " + pricePageTable.toString());
		System.out.println("'Page price table' size: " + pricePageTable.size());

		Map<String, BigDecimal> pricePageCommonTable = new Hashtable<String, BigDecimal>();
		pricePageCommonTable.put(coat.getName(), coat.getPrice());
		pricePageCommonTable.putAll(pricePageTable);

		System.out.println("'Common page price table' " + pricePageCommon.toString());
		System.out.println("'Common page price table' size: " + pricePageCommon.size());

		int countPriceTableUnderOne = 0;
		BigDecimal maxBigDecimalTable = new BigDecimal(0);
		BigDecimal minBigDecimalTable = new BigDecimal(Integer.MAX_VALUE);

		for (Map.Entry entry : pricePageTable.entrySet()) {
			BigDecimal entryBigDecimal = (BigDecimal) entry.getValue();
			if (entryBigDecimal.compareTo(oneBigDecimal) == -1) {
				countPriceTableUnderOne++;
			}
			if (entryBigDecimal.compareTo(maxBigDecimalTable) == 1) {
				maxBigDecimalTable = entryBigDecimal;
			}
			if (entryBigDecimal.compareTo(minBigDecimalTable) == -1) {
				minBigDecimalTable = entryBigDecimal;
			}

		}
		System.out.println("Price under 1 EUR in 'page price table' " + countPriceTableUnderOne);
		System.out.println("Max price in 'page price table'  " + maxBigDecimalTable.toString());
		System.out.println("Min price in 'page price table'  " + minBigDecimalTable.toString());

		List<BigDecimal> listTable = new ArrayList<>(pricePageTable.values());
		listTable.sort(null);
		System.out.println("Values of prices in 'page price table' in ascending order" + listTable);
	}

}

@Getter
@Setter
class Product {

	private String name;
	private BigDecimal price;

	public Product(String name, BigDecimal price) {
		this.name = name;
		this.price = price;
	}

}
