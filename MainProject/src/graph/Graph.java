package graph;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Graph {

//	private Set<Vertex> vertexes = new TreeSet<Vertex>();

	private Map<String, Vertex> vertexes = new TreeMap<String, Vertex>();// Key - name, Value - Vertex
	private Set<Edge> edges = new HashSet<Edge>();

	public static void main(String[] args) throws IOException{
	
		Graph graph = new Graph();
		graph.process();
		
	}

	// this will be the main method for the object
	public void process() {
		printElements(new HashSet<Object>(this.edges));
		System.out.println("There are " + this.edges.size() + " direct flights");
		System.out.println("Airports connected to Cape Town:");
		printElements(new HashSet<Object>(this.vertexes.get("Cape Town").getEdges()));
		System.out.println("There are " + this.vertexes.get("Jo'burg").getEdges().size() + " flights from Jo'burg");
	}

	public Graph() {
		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("Enter the number of vertexes: ");
		int numberOfVertexes = Integer.parseInt(sc.nextLine());

		// Create the vertexes
		for (int i = 0; i < numberOfVertexes; i++) {
			System.out.println("Enter the name for the vertex " + i);
			String name = sc.nextLine();// read the name of the vertex
			this.vertexes.put(name, (new Vertex(name)));
		}

		// Create the edge
		System.out.println("Enter the number of edges: ");
		int numberOfEdges = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < numberOfEdges; i++) {
			System.out.println("Edge: ");
			System.out.println("Enter the name of the vertex");
			String name1 = sc.nextLine();
			System.out.println("Enter the name of the vertex");
			String name2 = sc.nextLine();
			Vertex vertex1 = this.vertexes.get(name1);
			Vertex vertex2 = this.vertexes.get(name2);
			
			Edge edge = new Edge(vertex1, vertex2);
			this.edges.add(edge);
			vertex1.getEdges().add(edge);
			vertex2.getEdges().add(edge);			
		}

		sc.close();

	}

	public static void printElements(Collection<Object> element) {
		Iterator<Object> iterator = element.iterator();
		while (iterator.hasNext())
			System.out.println(iterator.next());
	}

}
