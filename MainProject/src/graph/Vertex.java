package graph;

import java.util.HashSet;
import java.util.Set;

public class Vertex implements Comparable<Vertex>{
	
//	private int index;
	private String name;
	private Set<Edge> edges = new HashSet<Edge>();
	
	public Vertex(String name) {
		this.name = name;
	}
	
	@Override
	public int compareTo(Vertex o) {
		return this.name.compareTo(o.name);
	}
	
	@Override
	public String toString() {
		return this.name;
	}

	public Set<Edge> getEdges() {
		return edges;
	}

	public void setEdges(Set<Edge> edges) {
		this.edges = edges;
	}	

}
