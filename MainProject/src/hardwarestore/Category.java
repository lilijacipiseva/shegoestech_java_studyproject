package hardwarestore;

import java.sql.PreparedStatement;

public class Category {

	private String name;
	private int id;

	public Category(String name) {
		this.name = name;
	}

	public void addStatusToDB() throws Exception {
		PreparedStatement st = FillDB.conn
				.prepareStatement("INSERT INTO hardwarestore.categories (name) VALUES (?)");
		st.setString(1, this.name);
		st.execute();
	}
}
