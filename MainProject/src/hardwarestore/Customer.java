package hardwarestore;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//
//
//import com.sun.source.util.TreePathScanner;
//
//import jdbctasks.OfficersDistrictDB;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Customer {
	
	private final String name;
	private final String surname;
	private final String email;
	private final String phoneNumber;
	private int id;
	
	
	public Customer(String name, String surname, String email, String phoneNumber) {
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	public void addCustomerToDB() throws Exception {
		PreparedStatement st = FillDB.conn
				.prepareStatement("INSERT INTO hardwarestore.customers (name,surname,email,phone_number) VALUES (?,?,?,?)");
		
		st.setString(1, this.name);
		st.setString(2, this.surname);
		st.setString(3, this.email);
		st.setString(4, this.phoneNumber);
		st.execute();
	}
}
