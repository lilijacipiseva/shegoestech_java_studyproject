package hardwarestore;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;


// Try to delete one record from JAVA code.

public class DeleteFromDB {
	protected static Connection conn;

	public static void main(String[] args) throws Exception {

		conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "student", "password");
		deleteAllFromTable("orders");
		deleteRecordFromTableById("products", 5);

	}

	private static void deleteAllFromTable(String tableName) throws Exception {
		Statement statement = DeleteFromDB.conn.createStatement();
		statement.execute("DELETE FROM hardwarestore." + tableName);
	}

	private static void deleteRecordFromTableById(String tableName, int id) throws Exception {
		Statement statement = DeleteFromDB.conn.createStatement();
		statement.execute("DELETE FROM hardwarestore." + tableName + " WHERE id = " + id);
	}

}
