package hardwarestore;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;
import java.util.TreeMap;
import java.util.ArrayList;
import java.util.List;

// Add 3 records at each table from JAVA code.

public class FillDB {
	protected static Connection conn;

	public static void main(String[] args) throws Exception {

		conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "student", "password");

		Customer customer1 = new Customer("Tom", "A", "tom@gmail.com", "1234");
		Customer customer2 = new Customer("Tom", "B", "tom@gmail.com", "1234");
		Customer customer3 = new Customer("Tom", "C", "tom@gmail.com", "1234");
		List<Customer> customers = new ArrayList<Customer>();
		customers.add(customer1);
		customers.add(customer2);
		customers.add(customer3);

		for (Customer customer : customers) {
			customer.addCustomerToDB();
		}

		Status status1 = new Status("prepered");
		Status status2 = new Status("received");
		Status status3 = new Status("changed");
		List<Status> statuses = new ArrayList<>();
		statuses.add(status1);
		statuses.add(status2);
		statuses.add(status3);

		for (Status status : statuses) {
			status.addStatusToDB();
		}

		Category keyboard = new Category("keyboard");
		Category mouse = new Category("mouse");
		Category monitor = new Category("monitor");
		List<Category> categories = new ArrayList<>();
		categories.add(keyboard);
		categories.add(mouse);
		categories.add(monitor);

		for (Category category : categories) {
			category.addStatusToDB();
		}

		Supplier samsung = new Supplier("Samsung", "samsung@gmail.com", "+37138786790");
		Supplier apple = new Supplier("Apple", "apple@gmail.com", "+37156467686");
		Supplier logitech = new Supplier("Logitech", "logitech@gmail.com", "+37143678922");
		List<Supplier> suppliers = new ArrayList<>();
		suppliers.add(samsung);
		suppliers.add(apple);
		suppliers.add(logitech);

		for (Supplier supplier : suppliers) {
			supplier.addSupplierToDB();
		}

		BigDecimal price = new BigDecimal("5.00");
		Product mouse1 = new Product("Bluetooth Mouse", "BL2021", price, 2);
		Product mouse2 = new Product("Optical Mouse", "OP2021", price, 2);
		Product mouse3 = new Product("Wireless Mouse", "WI2021", price, 2);
		List<Product> products = new ArrayList<>();
		products.add(mouse1);
		products.add(mouse2);
		products.add(mouse3);
		for (Product product : products) {
			product.setSupplierId(1);
			product.setCategoryId(6);
			product.addProductToDB();
		}

		Order order1 = new Order();
		order1.setProductId(1);
		order1.setCustomerId(1);
		order1.setStatusId(2);
		order1.setQuantity(1);
		Order order2 = new Order();
		order2.setProductId(2);
		order2.setCustomerId(2);
		order2.setStatusId(3);
		order2.setQuantity(1);
		Order order3 = new Order();
		order3.setProductId(3);
		order3.setCustomerId(3);
		order3.setStatusId(4);
		order3.setQuantity(1);
		List<Order> orders = new ArrayList<>();
		orders.add(order1);
		orders.add(order2);
		orders.add(order3);
		for (Order order : orders) {
			order.addOrderToDB();
		}
	}
}
