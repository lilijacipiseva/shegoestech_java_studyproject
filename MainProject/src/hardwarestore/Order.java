package hardwarestore;

import java.sql.PreparedStatement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Order {

	private int productId;
	private int customerId;
	private int statusId;
	private int id;
	private int quantity;

	public Order() {
	}

	public void addOrderToDB() throws Exception {
		PreparedStatement st = FillDB.conn.prepareStatement(
				"INSERT INTO hardwarestore.orders " + "(product_id,quantity,customer_id,status_id) VALUES (?,?,?,?)");
		st.setInt(1, this.productId);
		st.setInt(2, this.quantity);
		st.setInt(3, this.customerId);
		st.setInt(4, this.statusId);
		st.execute();
	}
}
