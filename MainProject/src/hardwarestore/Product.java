package hardwarestore;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.util.Currency;

import lombok.Getter;
import lombok.Setter;
@Getter@Setter
public class Product {

	private String name;
	private String description;
	private BigDecimal price; 
	private int categoryId;
	private int supplierId;
	private int id;
	private int warranty;

	public Product(String name, String description, BigDecimal price, int warranty) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.warranty = warranty;
	}

	public void addProductToDB() throws Exception {		
		PreparedStatement st = FillDB.conn
				.prepareStatement("INSERT INTO hardwarestore.products "
						+ "(name,description,price,warranty,category_id,supplier_id) VALUES (?,?,?,?,?,?)");
		st.setString(1, this.name);
		st.setString(2, this.description);
		st.setBigDecimal(3, this.price);
		st.setInt(4, warranty);
		st.setInt(5, categoryId);
		st.setInt(6, supplierId);
		st.execute();
	}
}
