package hardwarestore;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SelectFromDB {
	protected static Connection conn;

	public static void main(String[] args) throws Exception {

		conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "student", "password");

		selectNameFromStatus();
		selectDistinctNameFromCategories();
		selectThreeMostExpensiveProduct();
		selectAllOrders();
		selectProductsAndSuppliers();
		selectProductsAndCategories();

	}
//  Try to build 3 simple SELECT statements with WHERE, ORDER BY from JAVA code

	private static void selectNameFromStatus() throws Exception {
		Statement statement = SelectFromDB.conn.createStatement();

		ResultSet rs = statement.executeQuery("SELECT * FROM hardwarestore.statuses WHERE name = 'prepered'");

		while (rs.next()) {
			System.out.println("id: " + rs.getString(1) + " name: " + rs.getString(2));
		}

	}

	private static void selectDistinctNameFromCategories() throws Exception {
		Statement statement = SelectFromDB.conn.createStatement();

		ResultSet rs = statement.executeQuery("SELECT DISTINCT name FROM hardwarestore.categories");

		while (rs.next()) {
			System.out.println("name: " + rs.getString(1));
		}

	}

	private static void selectThreeMostExpensiveProduct() throws Exception {
		Statement statement = SelectFromDB.conn.createStatement();

		ResultSet rs = statement
				.executeQuery("SELECT name, price FROM hardwarestore.products ORDER BY price DESC LIMIT 3");

		while (rs.next()) {
			System.out.println("name: " + rs.getString(1) + " price: " + rs.getString(2));
		}

	}

// 	Try to build 3 advanced SELECT statements with JOIN from JAVA code.	

	private static void selectAllOrders() throws Exception {

		Statement statement = SelectFromDB.conn.createStatement();

		ResultSet rs = statement.executeQuery("SELECT hardwarestore.products.name, hardwarestore.orders.quantity, "
				+ "hardwarestore.customers.name," + " hardwarestore.customers.surname, "
				+ "hardwarestore.customers.email, " + "hardwarestore.customers.phone_number, "
				+ "hardwarestore.statuses.name " + "FROM hardwarestore.orders "
				+ "JOIN hardwarestore.products ON hardwarestore.products.id = hardwarestore.orders.product_id "
				+ "JOIN hardwarestore.statuses ON hardwarestore.statuses.id = hardwarestore.orders.status_id "
				+ "JOIN hardwarestore.customers on hardwarestore.orders.customer_id = hardwarestore.customers.id "
				+ " ORDER BY hardwarestore.statuses.name");

		while (rs.next()) {
			System.out.print("Product name: " + rs.getString(1) + ",");
			System.out.print("Quantity: " + rs.getInt(2) + ", ");
			System.out.print("Customer name: " + rs.getString(3) + ",");
			System.out.print("Customer surname: " + rs.getString(4) + ",");
			System.out.print("Customer e-mail: " + rs.getString(5) + ",");
			System.out.print("Customer phone: " + rs.getString(6) + ",");
			System.out.print("Order status: " + rs.getString(7) + ",");
			System.out.print(System.lineSeparator());
		}
	}

	private static void selectProductsAndSuppliers() throws Exception {

		Statement statement = SelectFromDB.conn.createStatement();

		ResultSet rs = statement.executeQuery("SELECT p.name, s.name FROM hardwarestore.products as p "
				+ "JOIN hardwarestore.suppliers as s ON p.supplier_id = s.id");

		while (rs.next()) {
			System.out.print("Product name: " + rs.getString(1) + ",");
			System.out.print("Supplier name: " + rs.getString(2));
			System.out.print(System.lineSeparator());
		}
	}

	private static void selectProductsAndCategories() throws Exception {

		Statement statement = SelectFromDB.conn.createStatement();

		ResultSet rs = statement.executeQuery("SELECT p.name, c.name FROM hardwarestore.products as p "
				+ "JOIN hardwarestore.categories as c ON p.category_id = c.id");

		while (rs.next()) {
			System.out.print("Product name: " + rs.getString(1) + ",");
			System.out.print("Category name: " + rs.getString(2));
			System.out.print(System.lineSeparator());
		}
	}

}
