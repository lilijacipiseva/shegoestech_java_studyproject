package hardwarestore;

import java.sql.PreparedStatement;

public class Status {

	private String name;
	private int id;

	public Status(String name) {
		this.name = name;
	}

	public void addStatusToDB() throws Exception {
		PreparedStatement st = FillDB.conn
				.prepareStatement("INSERT INTO hardwarestore.statuses (name) VALUES (?)");
		st.setString(1, this.name);
		st.execute();
	}
}
