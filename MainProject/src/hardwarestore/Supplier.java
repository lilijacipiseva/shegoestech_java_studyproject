package hardwarestore;

import java.sql.PreparedStatement;

public class Supplier {
	private String name, email, phoneNumber;
	private int id;

	public Supplier(String name, String email, String phoneNumber) {
		this.name = name;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	public void addSupplierToDB() throws Exception {
		PreparedStatement st = FillDB.conn
				.prepareStatement("INSERT INTO hardwarestore.suppliers (name,email,phone_number) VALUES (?,?,?)");

		st.setString(1, this.name);
		st.setString(2, this.email);
		st.setString(3, this.phoneNumber);
		st.execute();
	}

}
