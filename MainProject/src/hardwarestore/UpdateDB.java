package hardwarestore;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Try to update some records from JAVA code.

public class UpdateDB {
	protected static Connection conn;

	public static void main(String[] args) throws Exception {

		conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "student", "password");

		updateOrdersStatus("prepered", "delivered");
		updateStringRecordById("customers", "name", "Normunds", 4);

	}

	private static void updateOrdersStatus(String existingStatusName, String newStatusName) throws Exception {
		Statement statement = UpdateDB.conn.createStatement();

		int existingStatusID = 0;
		int newStatusID = 0;
		ResultSet resultStatuses = statement.executeQuery("SELECT * FROM hardwarestore.statuses");
		while (resultStatuses.next()) {
			if (resultStatuses.getString("name").equals(existingStatusName))
				existingStatusID = resultStatuses.getInt("id");
			else if (resultStatuses.getString("name").equals(newStatusName))
				newStatusID = resultStatuses.getInt("id");
		}

		List<Integer> idsToUpdate = new ArrayList<Integer>();
		ResultSet resultOrders = statement.executeQuery("SELECT * FROM hardwarestore.orders");
		while (resultOrders.next()) {
			if (resultOrders.getInt("status_id") == existingStatusID)
				idsToUpdate.add(resultOrders.getInt("id"));

		}

		Iterator<Integer> iterator = idsToUpdate.iterator();
		while (iterator.hasNext())
			statement.execute(
					"UPDATE hardwarestore.orders SET status_id = " + newStatusID + " WHERE id = " + iterator.next());

	}

	public static void updateStringRecordById(String tableName, String colomnName, String value, int id)
			throws Exception {
		Statement statement = UpdateDB.conn.createStatement();
		statement.execute(
				"UPDATE hardwarestore." + tableName + " SET " + colomnName + " = '" + value + "' WHERE id = " + id);
	}
}
