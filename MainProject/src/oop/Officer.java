package oop;



public class Officer {

	private String name, surname, workingDistrict;
	private int officerID, crimesSolved;

	public static void main(String[] args) {

		Officer officer1 = new Officer();
		Officer officer2 = new Officer();
		Officer officer3 = new Officer();

		officer1.setCrimasSolved(18);
		officer1.setName("Dwayne");
		officer1.setSurname("Johnson");
		officer1.setOfficerID(5489);
		officer1.setWorkingDistrict("Bruclin");

		officer2.setCrimasSolved(24);
		officer2.setName("John");
		officer2.setSurname("Statham");
		officer2.setOfficerID(2426);
		officer2.setWorkingDistrict("Bruclin");

		officer3.setCrimasSolved(34);
		officer3.setName("Richard");
		officer3.setSurname("Dreyfuss");
		officer3.setOfficerID(1287);
		officer3.setWorkingDistrict("Bruclin");

		System.out.println("The first officer: ");
		System.out.println(officer1);
		System.out.println(System.lineSeparator());

		System.out.println("The second officer: ");
		System.out.println(officer2);
		System.out.println(System.lineSeparator());

		System.out.println("The third officer: ");
		System.out.println(officer3);
		System.out.println(System.lineSeparator());
		
		Officer newOfficer = new Officer();
		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("Enter the name:");
		newOfficer.setName(sc.nextLine());
		System.out.println("Enter the surname:");
		newOfficer.setSurname(sc.nextLine());
		System.out.println("Enter the Officer ID:");
		newOfficer.setOfficerID(Integer.parseInt(sc.nextLine()));
		System.out.println("Enter the working district:");
		newOfficer.setWorkingDistrict(sc.nextLine());
		System.out.println("Enter crimes Solved:");
		newOfficer.setCrimasSolved(Integer.parseInt(sc.nextLine()));
		System.out.println(newOfficer);

		Officer[] district99 = new Officer[4];
		district99[0] = officer1;
		district99[1] = officer2;
		district99[2] = officer3;
		district99[3] = newOfficer;
		
		int countLevel1 = 0;
		int countLevelGreaterThan1 = 0;
		for (Officer officer : district99)
			if (officer.calculateLevel() == 1)
				countLevel1++;
			else
				countLevelGreaterThan1++;

		System.out.println("There are " + countLevel1 + " officers who have lavel 1");
//		System.out.println("There are " + countLevelGreaterThan1 + " officers who have lavel greater than 1");
		System.out
				.println("There are " + (district99.length - countLevel1) + " officers who have lavel greater than 1");

		boolean JohnIsFound = false;

		for (Officer officer : district99)
			if (officer.getName().equals("John")) {
				JohnIsFound = true;
				break;
			}
		
		//condition true/false ? (value if true) : (value if false)
		System.out.println(JohnIsFound ? "John is found" : "John is not found");

		
	
		
	}

	public Officer() {

	}

	public Officer(String name, String surname, String workingDistrict, int officerID, int crimesSolved) {
		this.name = name;
		this.surname = surname;
		this.crimesSolved = crimesSolved;
		this.officerID = officerID;
		this.workingDistrict = workingDistrict;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getWorkingDistrict() {
		return workingDistrict;
	}

	public void setWorkingDistrict(String workingDistrict) {
		this.workingDistrict = workingDistrict;
	}

	public int getOfficerID() {
		return officerID;
	}

	public void setOfficerID(int officerID) {
		this.officerID = officerID;
	}

	public int getCrimasSolved() {
		return crimesSolved;
	}

	public void setCrimasSolved(int crimesSolved) {
		this.crimesSolved = crimesSolved;
	}

	@Override
	public String toString() {
		return "Name: " + this.name + System.lineSeparator() + "Surname: " + this.surname + System.lineSeparator()
				+ "OfficerID: " + this.officerID + System.lineSeparator() + "Working district: " + this.workingDistrict
				+ System.lineSeparator() + "Crimes Solved: " + this.crimesSolved;
	}

	int calculateLevel() {
		if (this.crimesSolved < 20)
			return 1;
		else if (this.crimesSolved >= 20 && this.crimesSolved < 40)
			return 2;
		else
			return 3;
	}

}
