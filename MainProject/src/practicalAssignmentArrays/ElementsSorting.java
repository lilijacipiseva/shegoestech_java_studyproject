package practicalAssignmentArrays;

import java.util.Arrays;

import tools.ArrayTools;

/*
 Assignment No.4
Write a program where elements sorting will be performed.
Please take a look to the most popular sorting algorithms:
●	https://en.wikipedia.org/wiki/Quicksort
●	https://en.wikipedia.org/wiki/Merge_sort
●	https://en.wikipedia.org/wiki/Bubble_sort

Algorithms’ visualizations here:
https://www.cs.usfca.edu/~galles/visualization/ComparisonSort.html

Create an array with ten int elements: 10, 4, -4, 7, 0, 9, 1, 3, 7, -5 and try to sort all elements in ascending order.
Try to build the algorithm of Quick Sort, Merge Sort and Bubble Sort by Your own (without copying the code fragments from the internet). 

Which sorting algorithm is the best one for this case?

 How many loop structures need to be used in each sorting algorithm? 
 
 		int[] arrRandom = RandomArray.generateRandomArray(50);
		int[] arrRandomOrig = new int[10];
		System.arraycopy(arrRandom, 0, arrRandomOrig, 0, 10);
		Arrays.toString(arrRandom);
		System.out.println(Arrays.toString(arrRandom) + " arrNotSorted");

 */

public class ElementsSorting {

	public static void main(String[] args) {

//		int[] arr = { 10, 4, -4, 7, 0, 9, 1, 3, 7, -5 };

		int[] arr = { 7, 0, -6, 4, -2 };
//		bubbleSort(arr);
		ArrayTools.printArrayWithSemicolon(arr);
		System.out.println();

		quickSort(arr, 0, (arr.length - 1));

//		ArrayTools.printArrayWithSemicolon(arr);
	}

	public static void bubbleSort(int[] arr) {
		boolean sorted;
		int counter = 0;
		do {
			sorted = true;
			counter++;
			for (int i = 0; i < arr.length - 1; i++) {
				if (arr[i] > arr[i + 1]) {
					sorted = false;
					int sortVal = arr[i + 1];
					arr[i + 1] = arr[i];
					arr[i] = sortVal;
				}
			}

		} while (sorted == false);
//		System.out.println(counter + "counter");
//		return arr;
	}

	private int counter;

	/*
	 * 1. Pick an element, called a pivot, from the array. 2. Partitioning: reorder
	 * the array so that all elements with values less than the pivot come before
	 * the pivot, while all elements with values greater than the pivot come after
	 * it (equal values can go either way). After this partitioning, the pivot is in
	 * its final position. This is called the partition operation. 3. Recursively
	 * apply the above steps to the sub-array of elements with smaller values and
	 * separately to the sub-array of elements with greater values.
	 */

	public static void quickSort(int[] arr, int left, int right) {
//		if (arr == null || arr.length == 0)
//			return;
// 
//		if (low >= high)
//			return;

		// pick the pivot

		int middle = left + (right - left) / 2;
		int pivot = arr[middle];
		System.out.println(right + " right");
		System.out.println(left + " left");
		System.out.println(middle + " middle");
		System.out.println(pivot + " pivot");

		// make left < pivot and right > pivot
		int i = left, j = right;
		while (i <= j) {
			System.out.println("while 1");
			while (arr[i] < pivot) {
				i++;
				System.out.println("while 1.1. i = " + i);
			}

			while (arr[j] > pivot) {
				j--;
				System.out.println("while 1.2. j= " + j);

			}

			if (i <= j) {
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
				i++;
				System.out.println("if: j = " + j);
				j--;
			}
		}
		System.out.println(Arrays.toString(arr));
		System.out.println("i = " + i + "; j = " + j);
		System.out.println();
		// recursively sort two sub parts
		if (left < j)
			quickSort(arr, left, j);

		if (right > i)
			quickSort(arr, i, right);
	}
}
