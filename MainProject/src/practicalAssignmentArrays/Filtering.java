package practicalAssignmentArrays;
/*
 Assignment No.2
Write a program where multiple actions with an array will be done. For element filtering please use the loop (the loop type can be chosen as You want).
●	Create an array with ten int elements: 10, 4, -4, 7, 0, 9, 1, 3, 7, -5;
●	Print out elements which are negative. 
●	Print out elements which are odd (uneven). 
●	Compute how many elements are odd (uneven).
●	Verify if there are multiple elements with the same value
●	Print out each second element.
●	Compute average value of the array.
●	Establish how many elements are smaller than average value.
 */

public class Filtering {

	public static void main(String[] args) {
		int[] elements = { 10, 4, -4, 7, 0, 9, 1, 3, 7, -5 };

		for (int element : elements) {
			if (element < 0) {
				System.out.print(element);
				System.out.print(' ');
			}

		}

		System.out.print(System.lineSeparator());

		for (int element : elements) {
			if (Math.abs(element) % 2 == 1) {
				System.out.print(element);
				System.out.print(';');

			}
		}

		System.out.print(System.lineSeparator());

		int counter = 0;
		for (int element : elements) {
			if (Math.abs(element) % 2 == 1)
				counter++;
		}
		System.out.print("There are " + counter + " odd elements in the array");

		boolean sameElements = false;
		counter = 0;
		for (int i = 0; i < elements.length - 1; i++) {
			for (int j = i + 1; j < elements.length; j++) {
				if (elements[i] == elements[j]) {
					sameElements = true;
					counter++;
				}
			}
		}
		if (sameElements)
			System.out.print("There are " + counter + " same elements in the array");
		else
			System.out.print("No same elements in the array");

		System.out.print(System.lineSeparator());

				
		for (int i = 1; i < elements.length; i = i + 2) {
			System.out.print(elements[i]);
			System.out.print("; ");			
		}
		
		System.out.print(System.lineSeparator());		
		

		double sum = 0;
		for (int element : elements)
			sum += element;

		double averageVal = sum / elements.length;
		System.out.print("The average value is " + averageVal);
		
		System.out.print(System.lineSeparator());

		int smallerElements = 0;
		for (int element : elements) {
			if (element < averageVal)
				smallerElements++;
		}
		System.out.print(smallerElements + " elements are smaller than average value");

	}

}
