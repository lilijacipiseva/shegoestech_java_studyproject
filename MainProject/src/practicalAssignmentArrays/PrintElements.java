package practicalAssignmentArrays;
/*
 Assignment No.1
Write a program where array elements will be printed out:
Create an array with six int elements: 5, 3, 7, 6, 2, 8;
Print out each element using for loop;
Print out each element using for each loop;
Print out each element using while loop;
Print out each element using do while loop;

Which loop type is the best for this assignment? ==> For each!
 */

public class PrintElements {

	public static void main(String[] args) {
		int[] arr = { 5, 3, 7, 6, 2, 8 };

		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]);
			System.out.print("; ");
		}

		System.out.print(System.lineSeparator());

		for (int val : arr) {
			System.out.print(val);
			System.out.print(" ");
		}

		System.out.print(System.lineSeparator());

		int i = 0;

		while (i < arr.length) {
			System.out.print(arr[i]);
			System.out.print("_");
			i++;
		}

		System.out.print(System.lineSeparator());

		i = 0;
		do {
			System.out.print(arr[i]);
			System.out.print(": ");
			i++;
		} while (i < arr.length);

	}

}
