package strings;
/*
 Assignment No.1
Write a program to process String variables:
●	Create a String array with following values: “Computer”, “Plate”, “Chair”, “Girl”, “Boy”, “Cat”, “Dog”, “Shirt”, “Determination”;
●	Find out how many values are starting with ‘C’;
●	Find out how many values are ending with “e”;
●	Find out how many values are with length 5;
●	Find out how many values contain character “e”;
●	Find out is there any element which consists of the subString “te”;
●	* Calculate a histogram of values related to length of value.
 */

public class Asgmnt1 {

	public static void main(String[] args) {

		String[] arr = { "Computer", "Plate", "Chair", "Girl", "Boy", "Cat", "Dog", "Shirt", "Determination" };
		int countC = 0;
		int countE = 0;
		int countLen5 = 0;
		int countEs = 0;
		int countTeSubStr = 0;

		for (String value : arr) {
//			if (value.charAt(0) == 'C')
			if (value.startsWith("C"))
				countC++;
		}

		System.out.println("There are " + countC + " words starting with C");

		for (String value : arr) {
//			if (value.charAt(value.length()-1) == 'e')
			if (value.endsWith("e"))
				countE++;
		}

		System.out.println("There are " + countE + " words ending with e");

		for (String value : arr) {
			if (value.length() == 5)
				countLen5++;
		}

		System.out.println("There are " + countLen5 + " words, with length 5");

		for (String value : arr) {
			if (value.contains("e"))
				countEs++;
		}

		System.out.println("There are " + countEs + " words contain character e");

		for (String value : arr) {
			if (value.contains("te"))
				countTeSubStr++;
		}

		System.out.println("There are " + countTeSubStr + " words contain subString te");

		int[] lens = new int[30];

		for (String value : arr)
			lens[value.length()]++;

		for (int i = 0; i < lens.length; i++)
			System.out.println(i + " - " + lens[i]);
	}

}
