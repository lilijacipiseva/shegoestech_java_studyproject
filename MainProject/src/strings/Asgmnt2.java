package strings;
/*
 Assignment No.2
Write a program where multiple actions with a String will be performed:
●	Create a String “Climb mountains not so the world can see you, but so you can see the world”;
●	Take a look to the documentation of the String: https://docs.oracle.com/javase/7/docs/api/java/lang/String.html
●	Print out how many words are in the sentence;
●	Find out how many “the” words are in the sentence;
●	Find out how many words consists of the letter “s”;
●	Find out the words that are repeated in the sentence multiple times. For example, see, you, can, so, the;
●	Change the word “you” to “You”. Print out the new sentence.
 */

public class Asgmnt2 {

	public static void main(String[] args) {

		String str = "Climb mountains not so the world can see you, but so you can see the world";

		String[] arr = str.split(" ");
		System.out.println("The are " + arr.length + " words in the string");
//		printStringArray(arr);

		int countThe = 0;
		for (String val : arr) {
			if (val.replace(",", "").replace(":", "").replace(";", "").equals("the"))
				countThe++;
		}
		System.out.println("There are " + countThe + " wolds \"the\" in the sentence");

		int countS = 0;
		for (String val : arr) {
			if (val.replace(",", "").replace(":", "").replace(";", "").contains("s"))
				countS++;
		}
		System.out.println("There are " + countS + " words consists of the letter “s” in the sentence");

		for (int i = 0; i < arr.length; i++)
			for (int j = i + 1; j < arr.length; j++)
				if (arr[i].replace(",", "").replace(":", "").replace(";", "")
						.equals(arr[j].replace(",", "").replace(":", "").replace(";", "")))
					System.out.println("Word " + arr[i].replace(",", "").replace(":", "").replace(";", "")
							+ " is repiated multiple times");

		System.out.println(str.replace("you", "You"));

	}

	private static void printStringArray(String[] arr) {
		for (String str : arr) {
			System.out.println(str);
		}

	}

}
