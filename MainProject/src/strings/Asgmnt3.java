package strings;
/*
Assignment No.3
Write a program where actions with regex pattern will be provided:
●	Create a String variable name with the value “John” and create a 
regex pattern for that (only letters are allowed). Try to test it 
using matches() function;
●	Create a String variable username with the value “s20surname” and 
create a regex pattern for that (the first is a letter, after that 
2 digits are allowed and after that unlimited letters are allowed). 
Try to test it using matches() function;
●	Create a String variable personCodeOfLatvian with the value 
“121200-11311” and create a regex pattern for that (6 digits, after 
that - sign, after that 5 digits). Try to test it using matches() 
function.
 */

public class Asgmnt3 {

	public static void main(String[] args) {

		String name = "John";
		String pattern = "[A-Z]{1}[a-z]+";
		System.out.println(name.matches(pattern));

		String username = "s20surname";
		pattern = "[A-Za-z]{1}[0-9]{2}[A-Za-z]+";
		System.out.println(username.matches(pattern));

		String personCodeOfLatvian = "121200-11311";
		pattern = "[0-9]{6}[-]{1}[0-9]{5}";
		System.out.println(personCodeOfLatvian.matches(pattern));
	}

}
