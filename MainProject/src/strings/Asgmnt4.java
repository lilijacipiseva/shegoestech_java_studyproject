package strings;

/*
Assignment No.4
Write a program where actions with constant values of String 
(Enums) will be provided.
Please take a look here: https://www.geeksforgeeks.org/enum-in-java/
●	Create a new Enum where all card suits will be stored - Spades, 
Clubs, Hearts and Diamonds.
●	Create a new Enum where all card values will be stored - 
Two, Three, …, King and Ace.
●	Using two for loops, create a card pack as the String array with 
all possible cards, for example, Spades Two, Spades Three, Spades Four, etc. 
You can use Enum function values() if You want.
●	Create a shuffle algorithm and shuffle the card pack.
●	Select the first card and print it out in the console.
●	*Select another six cards and print all six cards in the console. 
It is not possible to select the same card from the pack because each 
card is unique. Please “remove” the card after selection. 
 */

public class Asgmnt4 {

	enum cardSuits {
		SPADES, CLUBS, HEARTS, DIAMONDS
	};

	enum cardValues {
		TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
	};

	public static void main(String[] args) {

		String[][] cardDeck = new String[cardSuits.values().length][cardValues.values().length];

		for (int i = 0; i < cardSuits.values().length; i++)
			for (int j = 0; j < cardValues.values().length; j++)
				cardDeck[i][j] = cardSuits.values()[i].name() + "-" + cardValues.values()[j].name();

//		print2DArray(cardDeck);

		for (int i = 0; i < cardSuits.values().length; i++)
			for (int j = 0; j < cardValues.values().length; j++) {
				int swapSuite = (int) (Math.random() * cardSuits.values().length);
				int swapValue = (int) (Math.random() * cardValues.values().length);
				String value = cardDeck[i][j];
				cardDeck[i][j] = cardDeck[swapSuite][swapValue];
				cardDeck[swapSuite][swapValue] = value;

			}

//		print2DArray(cardDeck);

		System.out.println("The first card selected is: " + cardDeck[0][0]);

		for (int i = 1; i < 7; i++)
			System.out.println("The " + i + ".th card is " + cardDeck[0][i]);

	}

	private static void print2DArray(String[][] arr) {
		for (int i = 0; i < arr.length; i++)
			for (int j = 0; j < arr[i].length; j++)
				System.out.println(arr[i][j]);
	}

}
