package strings;
/*
 Assignment No.5
Write a program where the verification of the palindrome is provided. Palindrome is the sentence which reads the same backward as forward. Please take a look at description of the palindrome: https://en.wikipedia.org/wiki/Palindrome
●	Create the algorithm which will process the sentence and return the result - is this sentence a palindrome or not.
●	Testing sentences:
a.	Mom
b.	Was it a car or a cat I saw?
c.	Madam, in Eden, I’m Adam.
d.	Yo, banana boy!
 */

import java.util.Scanner;

public class Asgmnt5 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the sentence: ");
		String sentence = sc.nextLine();
		System.out.println(sentence.replaceAll("[^a-zA-Z0-9]", "").equalsIgnoreCase(reverse(sentence)));
	}

	private static String reverse(String str) {

		String strRev = "";
		StringBuilder strBuild = new StringBuilder(str.replaceAll("[^a-zA-Z0-9]", ""));
		strBuild = strBuild.reverse();

		return strBuild.toString();
	}
		
}
